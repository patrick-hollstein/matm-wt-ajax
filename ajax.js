/* /////////////////////
 *
 * _ajax.js
 *
 * /////////////////////
*/

// TODO: Request nur senden, wenn online -> console.log(navigator.onLine)
// TODO: Fallback, wenn offline
// TODO: alle Statuscodes abfangen
// TODO: Error Report

function matm_ajax(options) {
	// set options
	options = options || {};

	let	opt_method	= options.method	|| 'POST',
		opt_url		= options.url		|| '',
		opt_success	= options.success	|| null,
		opt_failed	= options.failed	|| null,
		opt_async	= options.async		|| true,
		opt_data	= options.data		|| '';
	
	console.log('AJAX-Req to => ' + opt_url);

	// opt_data umformen
	let data = "" ;
	for (var key in opt_data) {
		data += "&" + key + "=" + opt_data[key];
	} // for

	// create XMLHttpRequest Object
	var xhttp = new XMLHttpRequest();

	// send request
	xhttp.open(opt_method, opt_url, opt_async);

	if(opt_method === 'POST') {
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(data);

		// handling receaved data
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				opt_success(this);
			}
		};
	} else {
		xhttp.send();

		// handling receaved data
		if(opt_success != null) {
			opt_success(this);
		}
	} // if

	
} // matm_ajax()